/**************************************************************************
 *  SettingsWindow.cpp
 *  Created 15/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#include "SettingsWindow.h"
#include "ui_SettingsWindow.h"
#include <QTableWidgetItem>
#include <QStringList>
#include <QCloseEvent>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

SettingsWindow::SettingsWindow(QWidget *parent) : QDialog(parent), ui(new Ui::SettingsWindow)
{
	ui->setupUi(this);

	dataPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);

	QStringList tableHeaders;
	tableHeaders << "Icon" << "Name" << "URL";
	ui->tableWidget->setHorizontalHeaderLabels(tableHeaders);

	streams = StreamHelper::parseStream(dataPath + "/streams.xml");
	ui->tableWidget->setRowCount(streams.size());
	ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
//	ui->tableWidget->setDragEnabled(true);
//	ui->tableWidget->setDragDropMode(QAbstractItemView::InternalMove);
//	ui->tableWidget->setDragDropOverwriteMode(false);

	for (int i = 0; i < (int)streams.size();i++)
	{
		Stream stream = streams.at(i);

		QTableWidgetItem *streamItemIcon = new QTableWidgetItem();
		streamItemIcon->setFlags(Qt::ItemIsDropEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		streamItemIcon->setIcon(QIcon(dataPath + "/icons/" + stream.icon));
		ui->tableWidget->setItem(i, 0, streamItemIcon);

		QTableWidgetItem *streamItemName = new QTableWidgetItem(stream.name);
		ui->tableWidget->setItem(i, 1, streamItemName);

		QTableWidgetItem *streamItemURL = new QTableWidgetItem(stream.url.toString());
		ui->tableWidget->setItem(i, 2, streamItemURL);
	}

	ui->tableWidget->resizeColumnsToContents();
	connect(ui->tableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(onCellChanged(int, int)));
	connect(ui->tableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(onCellDoubleClicked(int, int)));
}

SettingsWindow::~SettingsWindow()
{
	QLayoutItem *child;
	while ((child = ui->tableWidget->layout()->takeAt(0)) != 0)
			  delete child;

	delete ui->tableWidget;
	delete ui;
}

void SettingsWindow::closeEvent(QCloseEvent *event)
{
	StreamHelper::saveStreams(streams, dataPath + "/streams.xml");

	event->accept();
	emit finished(0);
}

void SettingsWindow::on_addButton_clicked()
{
	ui->tableWidget->insertRow(streams.size());
	Stream s;
	streams.push_back(s);
}

void SettingsWindow::on_removeButton_clicked()
{
	if (ui->tableWidget->selectedItems().length() <= 0)
	{
		QMessageBox::information(this, "No items selected", "Pease select items");
		return;
	}

	QTableWidgetItem* item = ui->tableWidget->selectedItems().at(0);
	Stream *s = &streams.at(item->row());

	QMessageBox::StandardButton reply;
	reply = QMessageBox::question(this, "Delete", "Do you want to delete " + s->name, QMessageBox::Yes | QMessageBox::No);
	if (reply == QMessageBox::Yes)
	{
		streams.erase(streams.begin() + item->row());
		ui->tableWidget->removeRow(item->row());
	}

}


void SettingsWindow::onCellChanged(int row, int column)
{
	QTableWidgetItem* item = ui->tableWidget->item(row, column);
	if (streams.size() < row)
		return;

	Stream *s = &streams.at(row);

	qDebug("Changed from %s to %s", s->name.toStdString().c_str(), item->text().toStdString().c_str());
	if (column == 1)
		s->name = item->text();

	if (column == 2)
		s->url = QUrl(item->text());
}

void SettingsWindow::onCellDoubleClicked(int row, int column)
{
	if (column != 0)
		return;

	QString fileName = QFileDialog::getOpenFileName(this, tr("Select Stream Icon"), "", tr("Image Files (*.png *.jpg *.bmp)"));

	if (fileName.isEmpty())
		return;

	Stream *s = &streams.at(row);

	QFileInfo fileInfo(fileName);
	if (QFile::copy(fileName, dataPath + "/icons/" + fileInfo.fileName()))
	{
		ui->tableWidget->item(row, column)->setIcon(QIcon(dataPath + "/icons/" + fileInfo.fileName()));

		if (QFile::exists(dataPath + "/icons/" + s->icon))
			QFile::remove(dataPath + "/icons/" + s->icon); // Remove old icon

		s->icon = fileInfo.fileName();
		qDebug("Change icon to %s", fileInfo.fileName().toStdString().c_str());
	}
}

