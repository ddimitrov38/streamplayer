/**************************************************************************
 *  SettingsWindow.h
 *  Created 15/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>
#include "StreamHelper.h"

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
	Q_OBJECT

public:
	explicit SettingsWindow(QWidget *parent = 0);
	~SettingsWindow();

private slots:
	void onCellChanged(int, int);
	void onCellDoubleClicked(int row, int column);
	void on_addButton_clicked();

	void on_removeButton_clicked();

private:
	std::vector<Stream> streams;
	QString dataPath;

	Ui::SettingsWindow *ui;
	void closeEvent(QCloseEvent *event);
};

#endif // SETTINGSWINDOW_H
