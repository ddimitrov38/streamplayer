/**************************************************************************
 *  MainWindow.cpp
 *  Created 11/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "SettingsWindow.h"
#include <QDir>
#include <QAction>
#include <QVideoWidget>
#include <QMessageBox>
#include <QStandardPaths>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	setWindowFlags(Qt::FramelessWindowHint);

	// Initialization
	dataPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
	QDir dataDir(dataPath);

	qDebug() << qApp->applicationDirPath();
	qDebug() << dataPath;

	if (!dataDir.exists())
	{
		qDebug("First lauch initialization");
		dataDir.mkdir(dataPath);
		dataDir.mkdir(dataPath + "/icons");

		// Copy initial streams
		QFile::copy(":/streams.xml", dataDir.absoluteFilePath("streams.xml"));
		QFile streamsFile(dataDir.absoluteFilePath("streams.xml"));
		streamsFile.setPermissions(QFile::ReadUser | QFile::WriteUser | QFile::ReadOwner | QFile::WriteOwner);

		// Copy initial icons
		QStringList icons = QDir(":/icons").entryList(QDir::Files, QDir::DirsFirst);
		for (QString icon : icons)
			QFile::copy(":/icons/" + icon, dataPath + "/icons/" + icon);
	}

	//Stream player
	player = new QMediaPlayer(this);
	player->setVideoOutput(ui->videoView);
	connect(player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(onStateChanged(QMediaPlayer::State)));
	connect(player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(onPlayerError(QMediaPlayer::Error)));
	connect(player, SIGNAL(metaDataChanged(const QString&,const QVariant&)), this, SLOT(onMetaDataChanged(const QString&,const QVariant&)));

	// Initialize menus
	trayIcon = new QSystemTrayIcon(QIcon(":/stream.png"), this);
#ifdef Q_OS_WIN // Handle single click for windows
	connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayActivated(QSystemTrayIcon::ActivationReason)));
#endif

	QAction *quitAction = new QAction(tr("Exit"), trayIcon );
	quitAction->setShortcut(QKeySequence("CTRL+SHIFT+X"));
	connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));

	QAction *hideAction = new QAction("Show/Hide", trayIcon);
	connect(hideAction, SIGNAL(triggered()), this, SLOT(onShowHide()));

	stopAction = new QAction("Stop", trayIcon);
	stopAction->setDisabled(true);
	connect(stopAction, SIGNAL(triggered()), this, SLOT(stopStream()));

	QAction *settingsAction = new QAction("Settings", trayIcon);
	connect(settingsAction, SIGNAL(triggered()), this, SLOT(showSettings()));


	QAction *aboutAction = new QAction("About", trayIcon);
	connect(aboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));

// Streams submenu
	streamsMenu = new QMenu("Streams");
	streamsActionGroup = new QActionGroup(this);
	streamsActionGroup->setExclusive(true);
	connect(streamsActionGroup, SIGNAL(triggered(QAction*)), this, SLOT(streamSelected(QAction*)));
	reloadStreams(0);

// Volume submenu
	QMenu *volumeMenu = new QMenu("Volume");
	QActionGroup *volumeGroup = new QActionGroup(this);
	volumeGroup->setExclusive(true);
	connect(volumeGroup, SIGNAL(triggered(QAction*)), this, SLOT(volumeSelected(QAction*)));
	for (int i = 100; i >= 0; i -= 25)
	{
		QAction *volumeAction = new QAction(QString::number(i).append("%"), volumeMenu);
		volumeAction->setData(QVariant(i)); // volume index
		volumeAction->setCheckable(true);
		volumeAction->setChecked(i==100?true:false);

		volumeGroup->addAction(volumeAction);
		volumeMenu->addAction(volumeAction);
	}

	QMenu *mainMenu = new QMenu;
	mainMenu->addAction(hideAction);
	mainMenu->addAction(stopAction);
	mainMenu->addMenu(streamsMenu);
	mainMenu->addMenu(volumeMenu);
	mainMenu->addAction(settingsAction);
	mainMenu->addAction(aboutAction);
	mainMenu->addAction(quitAction);

	trayIcon->setContextMenu(mainMenu);
	trayIcon->show();
}

void MainWindow::streamSelected(QAction* sender)
{
	Stream stream = streams.at(sender->data().toInt());

	player->stop();
	player->setMedia(stream.url);
	player->play();

	if (stream.type == STREAM_VIDEO)
	{
		show();
		raise();
		setFocus();
	}

	trayIcon->setToolTip(stream.name);
}

void MainWindow::volumeSelected(QAction* sender)
{
	player->setVolume(sender->data().toInt());
}


MainWindow::~MainWindow()
{
	trayIcon->contextMenu()->clear();

	delete ui;
	delete trayIcon;
	delete player;
}

void MainWindow::showAbout()
{
	QMessageBox::about(this, "About", tr("Audio/Video stream player\n"
										"Autor: Dimitar T. Dimitrov\n\n"
										"mitakatdd@gmail.com\n\n\n"
										"Sofia, Bulgaria 2016\n"));
}

void MainWindow::showSettings()
{
	SettingsWindow *settings = new SettingsWindow(this);
	connect(settings, SIGNAL(finished(int)), this, SLOT(reloadStreams(int)));
	settings->show();
	settings->raise();
	settings->setFocus();
}

void MainWindow::reloadStreams(int status)
{
	streams.clear();
	streams = StreamHelper::parseStream(dataPath + "/streams.xml");

	qDebug() << "Streams reload " << status;
	// To clear the menu actions
	streamsMenu->clear();
	streamsActionGroup->actions().clear();
	for (int i = 0; i < (int)streams.size(); i++)
	{
		Stream stream = streams.at(i);

		// Video listing
		if (i > 0 && stream.type == STREAM_VIDEO & streams.at(i - 1).type == STREAM_AUDIO)
			streamsMenu->addSeparator();

		QAction *streamAction = new QAction(stream.name, streamsMenu);
		streamAction->setIcon(QIcon( dataPath + "/icons/" + stream.icon));
		streamAction->setData(QVariant(i)); // stream index
		streamAction->setCheckable(true);
		streamAction->setChecked(false);

		streamsMenu->addAction(streamAction);
		streamsActionGroup->addAction(streamAction);
	}
}

void MainWindow::stopStream()
{
	player->stop();
	trayIcon->setToolTip("No media");
	hide();
}

void MainWindow::onShowHide()
{
	for (QString md : player->availableMetaData())
			qDebug() << md;

	if( isVisible() )
	{
		hide();
	}
	else if (player->isVideoAvailable() && player->state() == QMediaPlayer::PlayingState)
	{
		show();
		raise();
		setFocus();
	}
}

void MainWindow::trayActivated(QSystemTrayIcon::ActivationReason reason)
{
	if (reason == QSystemTrayIcon::Trigger)
	{
		trayIcon->contextMenu()->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
		trayIcon->contextMenu()->popup(trayIcon->geometry().topRight());
	}
}

void MainWindow::onMetaDataChanged(const QString &key,const QVariant &value)
{
	qDebug() << "Meta data changed " << key << " = " << value;
}

void MainWindow::onStateChanged(QMediaPlayer::State state)
{
	if (state == QMediaPlayer::StoppedState)
	{
		stopAction->setDisabled(true);
		hide();
		trayIcon->setIcon(QIcon(":/stream.png"));
	}
	else if (state == QMediaPlayer::PlayingState)
	{
		stopAction->setDisabled(false);
		trayIcon->setIcon(QIcon(":/stream_playing.png"));
	}
	else
	{
		trayIcon->setIcon(QIcon(":/stream_paused.png"));
	}
}

void MainWindow::onPlayerError(QMediaPlayer::Error error)
{
	QMessageBox::critical(this, "Media error", player->errorString());

	qDebug() << "Player error:" << error << player->errorString();
}

// Handle mouse
void MainWindow::mousePressEvent(QMouseEvent* event)
{
	mouseOldX = event->x();
	mouseOldY = event->y();
};

void MainWindow::mouseMoveEvent(QMouseEvent* event)
{
	move(event->globalX() - mouseOldX, event->globalY() - mouseOldY);
};

void MainWindow::mouseDoubleClickEvent(QMouseEvent* event)
{
	if (event->button() == Qt::LeftButton)
	{
		if (this->isFullScreen())
			setWindowState(Qt::WindowMaximized);
		else
			setWindowState(Qt::WindowFullScreen);
	}
}

void MainWindow::keyPressEvent(QKeyEvent *keyevent)
{
	// Pause/Resume
	if (keyevent->key() == Qt::Key_Space)
	{
		if (player->state() == QMediaPlayer::PausedState)
			player->play();
		else
			player->pause();
	}

	// Fullscreen
	if (keyevent->key() == Qt::Key_F)
	{
		if (this->isFullScreen())
			setWindowState(Qt::WindowMaximized);
		else
			setWindowState(Qt::WindowFullScreen);
	}

	// Stop
	if (keyevent->key() == Qt::Key_Escape)
	{
		stopStream();
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if (event)
		QApplication::quit();
}
