/**************************************************************************
 *  StreamHelper.cpp
 *  Created 15/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#include "StreamHelper.h"
#include <QDomDocument>
#include <QXmlStreamWriter>
#include <QFile>

std::vector<Stream> StreamHelper::parseStream(QString fileName)
{
	QDomDocument xml;
	QFile file(fileName);
	std::vector<Stream> ret;

	file.open(QIODevice::ReadOnly);
	xml.setContent(file.readAll());
	file.close();

	QDomElement docElem = xml.documentElement();

	QDomNode node = docElem.firstChild();
	while (!node.isNull())
	{
		QDomElement element = node.toElement();
		node = node.nextSibling();

		if (element.attribute("type").length() == 0)
			continue;

		Stream s;
		s.type = element.attribute("type") == "audio"?STREAM_AUDIO:STREAM_VIDEO;
		s.name = element.text();
		s.icon = element.attribute("icon");
		s.url = QUrl(element.attribute("url"));
		ret.push_back(s);
	}

	return ret;
}

void StreamHelper::saveStreams(std::vector<Stream> streams, QString fileName)
{
	QFile file(fileName);
	file.open(QIODevice::WriteOnly);

	QXmlStreamWriter xmlWriter(&file);
	xmlWriter.setAutoFormatting(true);
	xmlWriter.writeStartDocument();

	xmlWriter.writeStartElement("streams");

	for (Stream stream : streams)
	{
		xmlWriter.writeStartElement("stream");
		xmlWriter.writeAttribute("type", stream.type==STREAM_AUDIO?"audio":"video");
		xmlWriter.writeAttribute("url", stream.url.toString());
		xmlWriter.writeAttribute("icon", stream.icon);
		xmlWriter.writeCharacters(stream.name);
		xmlWriter.writeEndElement();
	}

	xmlWriter.writeEndElement();
	file.close();
}
