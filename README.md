# Audio/Video stream player from system tray #

 A small project I made for myself. I am sharing it with you now.

### Some information ###

* Uses QT and QMediaPlayer for the stream so for some streams you might need additional video codec on Windows.
* You can add your own streams from the 'Settings' menu
* To view the video in fullscreen double click on the video window or press 'F'
* To pause the video press 'SPACEBAR'
* To close the video press 'ESC'

## Screenshots ##

![OSX Screenshot](https://bytebucket.org/ddimitrov38/streamplayer/raw/2ea5ca0c989411b0db91a0ad6af3aed30146c931/DOCS/mac_screenshot1.png)

![Windows screenshot](https://bytebucket.org/ddimitrov38/streamplayer/raw/bd259d09893e21da73b6e4ad6a3fe61e8badacad/DOCS/win_screenshot.png)