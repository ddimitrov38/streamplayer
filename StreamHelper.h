/**************************************************************************
 *  StreamHelper.h
 *  Created 15/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#ifndef STREAMHELPER_H
#define STREAMHELPER_H

#include <vector>
#include <QUrl>
#include <QString>

typedef enum
{
	STREAM_AUDIO = 0,
	STREAM_VIDEO = 1
} StreamType;

typedef struct
{
	StreamType type;
	QString name;
	QUrl url;
	QString icon;
} Stream;

class StreamHelper
{
public:
	StreamHelper();
	static std::vector<Stream> parseStream(QString fileName);
	static void saveStreams(std::vector<Stream> streams, QString fileName);
};

#endif // STREAMHELPER_H
