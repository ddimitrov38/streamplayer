/**************************************************************************
 *  MainWindow.h
 *  Created 11/5/2016 by Dimitar T. Dimitrov
 *  mitakatdd@gmail.com
 *
 *  MIT License
 *
 **************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "StreamHelper.h"
#include <QSystemTrayIcon>
#include <QMediaPlayer>
#include <QUrl>
#include <QMouseEvent>
#include <QMenu>

namespace Ui {
	class MainWindow;
}
class QString;
class QSystemTrayIcon;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void onShowHide();
	void showAbout();
	void stopStream();
	void showSettings();
	void reloadStreams(int status);
	void streamSelected(QAction* sender);
	void volumeSelected(QAction* sender);

	void onStateChanged(QMediaPlayer::State state);
	void onMetaDataChanged(const QString &key,const QVariant &value);
	void onPlayerError(QMediaPlayer::Error error);
	void trayActivated(QSystemTrayIcon::ActivationReason reason);

private:
	QMediaPlayer* player;
	Ui::MainWindow *ui;
	QAction *stopAction;
	QSystemTrayIcon* trayIcon;
	QMenu *streamsMenu;
	QActionGroup *streamsActionGroup;
	QString dataPath;

	std::vector<Stream> streams;

	int mouseOldX;
	int mouseOldY;

	void mousePressEvent(QMouseEvent* event);
	void mouseMoveEvent(QMouseEvent* event);
	void mouseDoubleClickEvent(QMouseEvent* event);

	void keyPressEvent(QKeyEvent *keyevent);
	void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
