#-------------------------------------------------
#
# Project created by QtCreator 2016-05-11T09:17:11
#
#-------------------------------------------------

QT += core gui xml multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = StreamPlayer
TEMPLATE = app

macx {
    ICON = icon.icns
    QMAKE_INFO_PLIST = Info.plist
}

win32 {
    RC_ICONS = icon.ico
}


SOURCES += main.cpp\
        MainWindow.cpp \
    SettingsWindow.cpp \
    StreamHelper.cpp

HEADERS  += MainWindow.h \
    SettingsWindow.h \
    StreamHelper.h

FORMS    += MainWindow.ui \
    SettingsWindow.ui

RESOURCES += \
    resources.qrc
